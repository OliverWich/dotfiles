./install.ps1

Write-Host "Launching elevated PWSH to create Symlinks for config files..." -ForegroundColor DarkGreen

Start-Process pwsh -WindowStyle Minimized -Verb runas -ArgumentList "-NoExit -File ./configure.ps1"

Write-Host "Once the process in the minimized window finishes, everything is Done!" -ForegroundColor DarkGreen
Write-Host "Now check the READNE for manual ToDos!" -ForegroundColor DarkRed
