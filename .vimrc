" Don't try to be vi compatible
set nocompatible

" Helps force plugins to load correctly when it is turned back on below
filetype off

" TODO: Load plugins here (pathogen or vundle)

" Turn on syntax highlighting
syntax on
colorscheme slate
set t_Co=256

" For plugins to load correctly
filetype plugin indent on

" Use comma as leader key
let mapleader = ","

" Security
set modelines=0

" Show line numbers
set number

" Show file stats
set ruler

" Encoding
set encoding=utf-8

" Whitespace
set wrap                  " Enable line wrapping
set textwidth=79          " Set maximum text width for line wrapping at 79 characters
set formatoptions=tcqrn1  " Set formatting options for text and comments
set tabstop=4             " Set tab width to 4 spaces
set shiftwidth=4          " Set indentation width to 4 spaces
set softtabstop=4         " Set number of spaces for a tab press to 4
set expandtab             " Convert tabs to spaces
set noshiftround          " Prevent automatic rounding of indentations to `shiftwidth`

" Cursor motion
set scrolloff=3           " Keep 3 lines visible above/below the cursor when scrolling
set backspace=indent,eol,start " Allow backspace over indents, line breaks, and insertion start
set matchpairs+=<:>       " Add angle brackets to % matching
runtime! macros/matchit.vim " Load enhanced matching for complex structures

" Move up/down editor lines
nnoremap j gj             " Remap `j` to move down display lines
nnoremap k gk             " Remap `k` to move up display lines

" Allow hidden buffers
set hidden                " Allow switching between unsaved files

" Rendering
set ttyfast               " Optimize for fast terminals

" Status bar
set laststatus=2          " Always show status line

" Last line
set showmode              " Show current mode (e.g., -- INSERT --)
set showcmd               " Show incomplete commands in the status line

" Searching
nnoremap / /\v            " Use `\v` (magic mode) for simpler regex in searches
vnoremap / /\v            " Same as above, but in visual mode
set hlsearch              " Highlight all search matches
set incsearch             " Show search matches as you type
set ignorecase            " Make searches case-insensitive
set smartcase             " Override `ignorecase` if the search contains uppercase letters
set showmatch             " Highlight matching pairs of brackets, parentheses, etc.
map <leader><space> :let @/=''<cr> " Clear search highlighting with <leader><space>

" Remap help key.
inoremap <F1> <ESC>:set invfullscreen<CR>a " Toggle fullscreen with F1 in insert mode
nnoremap <F1> :set invfullscreen<CR>       " Toggle fullscreen with F1 in normal mode
vnoremap <F1> :set invfullscreen<CR>       " Toggle fullscreen with F1 in visual mode

" Formatting
map <leader>q gqip        " Format the current paragraph with <leader>q

" Visualize tabs and newlines
set listchars=tab:▸\ ,eol:¬
" Use your leader key + l to toggle on/off
map <leader>l :set list!<CR> " Toggle tabs and EOL

" Set working directory to current folder in tree
set autochdir

" Tree File Structure using netrw (Hint: use ctrl+w+w to switch between windows)
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25
augroup ProjectDrawer
    autocmd!
    " open the tree sidebar automatically when vim is started
    autocmd VimEnter * :Vexplore
    " automatically focus the opened file if vim was opened with vim <file>
    autocmd VimEnter * if (argc() > 0 && filereadable(argv()[0])) | wincmd l | endif
augroup END

" quick Quit
command Q qa
" quick Quit without save"
command FQ qa!
" quick Save and Quit
command S wqa
