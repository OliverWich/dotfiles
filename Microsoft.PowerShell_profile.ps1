# Theming
# Theme based on 'pure'
oh-my-posh init pwsh --config $ENV:USERPROFILE\.oliverwich.omp.yml | Invoke-Expression


# Brezel helpers
function runUifBrezel {
	if (!(Test-Path ./bin/u.ps1)) {
		Write-Host "Can't run bin/u as this is not a brezel directory!" -ForegroundColor DarkRed
		return
	}
	./bin/u.ps1
}
function runLifBrezel {
	if (!(Test-Path ./bin/l.ps1)) {
		Write-Host "Can't run bin/l as this is not a brezel directory!" -ForegroundColor DarkRed
		return
	}
	./bin/l.ps1
}
function runLAifBrezel {
	if (!((Test-Path ./bin/l.ps1) -and (Test-Path ./bin/apply.ps1))) {
		Write-Host "Can't run bin/l & bin/a as this is not a brezel directory!" -ForegroundColor DarkRed
		return
	}
	./bin/l.ps1
	./bin/apply.ps1
}
function runApplyifBrezel {
	if (!(Test-Path ./bin/apply.ps1)) {
		Write-Host "Can't run bakery:apply as this is not a brezel directory!" -ForegroundColor DarkRed
		return
	}
	./bin/apply.ps1
}
function runMFSifBrezel {
	if (!(Test-Path ./bin/mfs.ps1)) {
		Write-Host "Can't run bin/mfs as this is not a brezel directory!" -ForegroundColor DarkRed
		return
	}
	./bin/mfs.ps1
}


# Aliases
function wt. { wt -d . } #Windows terminal in current dir
function nano($file) { & 'C:\Program Files\Git\usr\bin\nano.exe' $file }
function edit($file) { idea --temp-project $file } # Edit via temporary project with IntelliJ
function light($file) { idea -e $file } # Light edit with IntelliJ
# Brezel
Set-Alias -Name mfs -Value runMFSifBrezel
Set-Alias -Name u -Value runUifBrezel
Set-Alias -Name l -Value runLifBrezel
Set-Alias -Name load -Value runLifBrezel
Set-Alias -Name la -Value runLAifBrezel
Set-Alias -Name a -Value runApplyifBrezel
Set-Alias -Name apply -Value runApplyifBrezel
# Gpg
function restartgpg { gpg-connect-agent reloadagent /bye }

# Helpers for Dev drive
$code = 'x:'

# History / Autocomplete
Import-Module PSReadline

# only show history starting with current input when pressing Ctrl while using arrows
Set-PSReadLineKeyHandler -Key Control+UpArrow -Function HistorySearchBackward
Set-PSReadLineKeyHandler -Key Control+DownArrow -Function HistorySearchForward

Set-PSReadLineOption -PredictionSource History
# Only use ListView History if window is big enough
$WindowWidth = (Get-Host).UI.RawUI.MaxWindowSize.Width
$WindowHeight = (Get-Host).UI.RawUI.MaxWindowSize.Height
if ($WindowWidth -gt 64 -And $WindowHeight -gt 16) {
	set-psreadlineoption -PredictionViewStyle ListView
} else {
	set-psreadlineoption -PredictionViewStyle InlineView
}

# Command to reload env variables
function refreshenv {
	$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") +
			";" +
			[System.Environment]::GetEnvironmentVariable("Path","User")
}
