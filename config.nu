# config.nu
#
# Installed by:
# version = "0.102.0"
#
# This file is used to override default Nushell settings, define
# (or import) custom commands, or run any other startup tasks.
# See https://www.nushell.sh/book/configuration.html
#
# This file is loaded after env.nu and before login.nu
#
# You can open this file in your default editor using:
# config nu
#
# See `help config nu` for more options
#
# You can remove these comments if you want or leave
# them for future reference.

# Hide banner
$env.config.show_banner = false

# Brezel helpers
def run-u-if-brezel [] {
    if not ($env.PWD | path join "bin/u.ps1" | path exists) {
        print $"(ansi red_bold)Can't run bin/u as this is not a brezel directory!(ansi reset)"
        return
    }
    ^./bin/u.ps1
}

def run-l-if-brezel [] {
    if not ($env.PWD | path join "bin/l.ps1" | path exists) {
        print $"(ansi red_bold)Can't run bin/l as this is not a brezel directory!(ansi reset)"
        return
    }
    ^./bin/l.ps1
}

def run-la-if-brezel [] {
    if not (($env.PWD | path join "bin/l.ps1" | path exists) and ($env.PWD | path join "bin/apply.ps1" | path exists)) {
        print $"(ansi red_bold)Can't run bin/l & bin/a as this is not a brezel directory!(ansi reset)"
        return
    }
    ^./bin/l.ps1
    ^./bin/apply.ps1
}

def run-apply-if-brezel [] {
    if not ($env.PWD | path join "bin/apply.ps1" | path exists) {
        print $"(ansi red_bold)Can't run bakery:apply as this is not a brezel directory!(ansi reset)"
        return
    }
    ^./bin/apply.ps1
}


# Utility functions
def nano [file: string] { ^"C:/Program Files/Git/usr/bin/nano.exe" $file } # Using forward slashes
def edit [file: string] { ^idea --temp-project $file } # Edit via temporary project with IntelliJ
def light [file: string] { ^idea -e $file } # Light edit with IntelliJ
def restart-gpg [] { ^gpg-connect-agent reloadagent /bye }

# Windows terminal in current dir via `wt .`
def wt [dir?: string] {
    if $dir == "." {
        ^wt -d .
    } else if $dir == null {
        ^wt
    } else {
        ^wt -d $dir
    }
}

# Aliases
alias u = run-u-if-brezel
alias l = run-l-if-brezel
alias load = run-l-if-brezel
alias la = run-la-if-brezel
alias a = run-apply-if-brezel
alias apply = run-apply-if-brezel

# Command to reload env variables
def refreshenv [] {
    if ($nu.os-info.name == "windows") {
        # Get both Machine and User PATH variables
        let machine_path = (^powershell -noprofile -command "[System.Environment]::GetEnvironmentVariable('Path','Machine')" | str trim)
        let user_path = (^powershell -noprofile -command "[System.Environment]::GetEnvironmentVariable('Path','User')" | str trim)

        # Combine and update PATH
        load-env { PATH: $"($machine_path);($user_path)" }
        print "PATH environment refreshed from registry"
    } else {
        # Unix-like systems approach
        let system_path = (^bash -c "echo $PATH" | str trim)
        load-env { PATH: $system_path }
        print "PATH environment refreshed"
    }
}

# Plugins
# prepare temp dir
mkdir ($nu.data-dir | path join "vendor/autoload")

# Starship
#starship init nu | save -f ($nu.data-dir | path join "vendor/autoload/starship.nu")

# Carapace shell completion
carapace _carapace nushell | save -f ($nu.data-dir | path join "vendor/autoload/carapace.nu")
